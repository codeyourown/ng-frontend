import {Component} from '@angular/core';
import {AuthService} from '../Services/auth/auth.service';
import {take} from 'rxjs/operators';
import {NgForm} from '@angular/forms';
import {AuthState} from '../Services/auth/authState.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent {

  statusMessage: string = '';

  constructor(private authService: AuthService, private router: Router) { }

  login(form: NgForm): void {
    this.authService.login(form).pipe(take(1)).subscribe((authState: AuthState) => {
      if (authState === AuthState.SUCCESS) {
        this.statusMessage = '';
        this.router.navigate(['dashboard']).then(r => console.log('Nav-Result:', r));
        return;
      }
      switch (authState) {
        case AuthState.FAILED:
          this.statusMessage = 'E-Mail/Password wrong';
          break;
        case AuthState.INVALID:
          this.statusMessage = 'Login-Form invalid';
          break;
      }
      console.log(this.statusMessage);
    });
  }

}
