import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from './storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl: string = 'http://localhost:8080';

  constructor(private httpClient: HttpClient,
              private storage: StorageService
  ) { }

  private getJwt(): string | null {
    return this.storage.get('jwt');
  }

  get<F = any>(url: string): Observable<F> {
    const jwt = this.getJwt() ?? '';
    return this.httpClient.get<F>(this.baseUrl + url, {headers: {Authorization: 'Bearer ' + jwt}});
  }

  post<T = any | null, F = any>(url: string, body: T): Observable<F> {
    const jwt = this.getJwt() ?? '';
    return this.httpClient.post<F>(this.baseUrl + url, body, {headers: {Authorization: 'Bearer ' + jwt}});
  }

  put<T = any, F = any>(url: string, body: T): Observable<F> {
    const jwt = this.getJwt() ?? '';
    return this.httpClient.put<F>(this.baseUrl + url, body, {headers: {Authorization: 'Bearer ' + jwt}});
  }

  delete<F = any>(url: string): Observable<F> {
    const jwt = this.getJwt() ?? '';
    return this.httpClient.delete<F>(this.baseUrl + url, {headers: {Authorization: 'Bearer ' + jwt}});
  }
}
