import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  save(key: string, value: string): void {
    if (!key || key === '') {
      return;
    }
    localStorage.setItem(key, value);
  }

  get(key: string): string | null {
    if (!key || key === '') {
      return null;
    }
    return localStorage.getItem(key);
  }
}
