export enum AuthState {
  'INVALID',
  'FAILED',
  'SUCCESS'
}
