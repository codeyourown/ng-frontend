import {Injectable} from '@angular/core';
import {HttpService} from '../http.service';
import {AuthRequest} from './auth-request';
import {AuthResponse} from './auth-response';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {NgForm} from '@angular/forms';
import {StorageService} from '../storage/storage.service';
import {AuthState} from './authState.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authenticatedSubject: Subject<boolean> = new BehaviorSubject<boolean>(this.checkToken());
  authenticated$: Observable<boolean> = this.authenticatedSubject.asObservable();

  constructor(private http: HttpService, private storage: StorageService) {}

  private checkToken(): boolean {
    const jwt: string = this.storage.get('jwt') ?? '';
    return jwt.length > 0;
  }

  checkAuthentication(): boolean {
    const authenticated: boolean = this.checkToken();
    this.authenticatedSubject.next(authenticated);
    return authenticated;
  }

  login(form: NgForm): Observable<AuthState> {
    if (!form || !form.valid || !form.value?.mail || !form.value?.password) {
      console.log('Form invalid!');
      this.authenticatedSubject.next(false);
      return of(AuthState.INVALID);
    }
    const authRequest: AuthRequest = {
      mail: form.value?.mail,
      password: form.value?.password
    };
    return this.http
      .post<AuthRequest, AuthResponse>('/authenticate/login', authRequest)
      .pipe(
        map((authResponse: AuthResponse) => {
          if (!authResponse?.jwt || authResponse?.jwt?.length < 5) {
            this.authenticatedSubject.next(false);
            return AuthState.FAILED;
          }
          this.storage.save('jwt', authResponse.jwt);
          this.authenticatedSubject.next(true);
          return AuthState.SUCCESS;
        }),
      );
  }
}
