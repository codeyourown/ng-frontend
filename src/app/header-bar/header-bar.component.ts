import {Component, OnInit} from '@angular/core';
import {AuthService} from '../Services/auth/auth.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent {
  authenticated: Observable<boolean> = this.authService.authenticated$;

  constructor(private authService: AuthService) {
    this.authenticated = this.authService.authenticated$;
    this.authenticated.subscribe(console.log);
  }
}
